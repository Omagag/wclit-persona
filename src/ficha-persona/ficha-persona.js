import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona.js';

class FichaPersona  extends LitElement {

  static get styles() {
    return css`
      div {
        border: 1px solid;
        border-radius: 10px;
        padding: 10px;
        margin: 10px;
        width: 400px
      }
    `;
  }

  static get properties() {
    return {
        nombre:{type: String},
        apellidos:{type: String},
        aniosAntiguedad:{type: Number},
        nivel: {type: String},
        foto:{type: Object},
        origen:{type: String},
        bg:{type: String}
    };
  }

  constructor() {
    super();
    this.nombre = "Pedro";
    this.apellidos = "Lopez Lopez",
    this.aniosAntiguedad = 4;
    this.foto = {
        src: "./src/ficha-persona/img/persona.jpeg",
        alt: "Foro persona"
    }
    this.bg = "aliceblue";
  }

  render() {
    return html`
      <div style="background-color:${this.bg}">
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" value="${this.nombre}" @input="${this.updateNombre}"/>
        <!-- <input type="text" id="nombre" name="nombre" .value="${this.nombre}"/> -->
        <br/>
        <label for="apellidos">Apellidos</label>
        <input type="text" id="apellidos" name="apellidos" value="${this.apellidos}"/>
        <br/>
        <label for="aniosAntiguedad">Años antiguedad</label>
        <input type="text" id="aniosAntiguedad" name="aniosAntiguedad" value="${this.aniosAntiguedad}" @input="${this.updateAntiguedad}"/>
        <br/>
        <label for="nivel">Nivel</label>
        <input type="text" id="nivel" name="nivel" value="${this.nivel}" disabled/>
        <br/>
        <origen-persona @origin-setted="${this.origenChanged}"></origen-persona>
        <br/>
        <img src="${this.foto.src}" height="200" widtg="200" alt="${this.foto.alt}"/>
        <br/>
      </div>
    `;
  }
  updated(changedProperties) {
    if (changedProperties.has("nombre")) {
        console.log("Nombre modificado. " 
            + changedProperties.get("nombre") + " " + this.nombre);
    }
    if (changedProperties.has("aniosAntiguedad")) {
        this.actualizarNivel();
    }
  }
  updateNombre(e) {
    this.nombre = e.target.value;
  }
  updateAntiguedad(e) {
    this.aniosAntiguedad = e.target.value;
  }
  actualizarNivel() {
    if (this.aniosAntiguedad >= 7) {
      this.nivel = "Líder";
    } else if (this.aniosAntiguedad >= 5) {
      this.nivel = "Senior";
    } else if (this.aniosAntiguedad >= 3) {
      this.nivel = "Team";
    } else {
      this.nivel = "Junior";
    }
  }
  origenChanged(e) {
    this.origen = e.detail.value;
    if (this.origen === 'USA') {
      this.bg = "pink";
    } else if (this.origen === 'México') {
      this.bg = "lightgreen";
    } else if (this.origen === 'Canada') {
      this.bg = "lightyellow";
    }
  }
}

customElements.define('ficha-persona', FichaPersona);