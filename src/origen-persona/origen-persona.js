import { LitElement, html, css } from 'lit-element';

export class OrigenPersona  extends LitElement {

  static get styles() {
    return css`
      select {
        width: 200px;
      }
    `;
  }

  static get properties() {
    return {
        origenes: {type: Array}
    };
  }

  constructor() {
    super();
    this.origenes = ["México", "USA", "Canada"];
  }

  render() {
    return html`
        <label for="origenes">Origen</label>
        <select id="origenes" name="origenes" @change="${this.selChange}">
            ${this.origenes.map(i => html`<option>${i}</option>`)}
        </select>
    `;
  }
  selChange(e) {
    let value = this.shadowRoot.querySelector("#origenes").value;
    let event = new CustomEvent("origin-setted", 
                                {detail: {value: value}});
    this.dispatchEvent(event);
  }
}

customElements.define('origen-persona', OrigenPersona);